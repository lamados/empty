package empty

// Type is an empty type.
type Type = struct{}

// Value is an empty value.
var Value = Type{}

// Deduplicate takes in input slice and returns a copy with all duplicates removed.
// When the slice has fewer than 3 elements the returned slice will be ordered the same as the input. In all other cases assume the resulting slice will be a different order to the input.
func Deduplicate[T comparable](slice []T) []T {
	switch len(slice) {
	case 0, 1:
		return slice
	case 2:
		if slice[0] == slice[1] {
			return slice[:1]
		}
		return slice
	default:
		m := make(map[T]Type)
		for _, v := range slice {
			m[v] = Value
		}
		s := make([]T, 0, len(m))
		for v := range m {
			s = append(s, v)
		}
		return s
	}
}
